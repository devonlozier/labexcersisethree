
// MadLibs Lab Excersise
// Devon Lozier, Daniel Mader, Devon Norman

#include <conio.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;


void MadLib(string lib[10], string libanswers[10], ostream& os = cout)
{
	for (size_t i = 0; i < 10; i++)
	{
		cout << lib[i] << libanswers[i];
	}

}


int main()
{
	//Establishes a ofstream ofs for madlib.txt
	ofstream ofs("madlib.txt");
	string save = " "; // stores char value to determine to save or not.

	//Tells user what kind of inputs they should enter
	string madlibtypes[10]
	{
	"Silly Word", "Last Name", "Illness", "Noun (Plural)", "Adjective","Adjective", "Silly Word", "Place", "Number", "Adjective"
	};

	//Variable that holds how many answers.
	string madlibAnswers[10];

	string madlib[10]{ "","", " will not be attending school today.He / she has come down with a case of "
	," and has horrible "," and a/an ", " fever. We have made an appointment with the "," Dr. ",", who studied for many years in "
	," and has ", "degrees in pediatrics. He will send you all the information you need. Thank you! Sincerely Mrs. " };

	for (size_t i = 0; i < 10; i++)
	{
		cout << "Enter a " << madlibtypes[i] << "\n";
		cin >> madlibAnswers[i];
	}
	for (size_t i = 0; i < 10; i++)
	{
		cout << madlib[i] << madlibAnswers[i];
	}

	//Calls madLib function and pulls in arrays of madLib and madLibAnswers
	MadLib(madlib, madlibAnswers);
	cout << "\n" << endl; //Placeholder formatting (?)

	//Prompts user to input a value.
	cout << "Would you like to save to a file?\n" << "(Y/N)\n";
	cin >> save;

	//Loop to check if the value is equal to the yes option.
	if (save == "Y" || save == "y")
	{
		//Checks if ofs is enabled.
		if (ofs)
		{
			//Loops for each ofs for madlib and madlibAwnsers.
			for (size_t i = 0; i < 10; i++)
			{
				ofs << madlib[i] << madlibAnswers[i];
			}
		}
		//Closes OFS instance.
		ofs.close();
	}

	(void)_getch();
	return 0;
}




